## Name
EasyCreate - A large, yet functional modpack using packwiz

## Description
This is a modpack I made for my friend, but you should be able to install it using the instructions provided.

## Client Installation

- Download Prism Launcher from [here](https://github.com/PrismLauncher/PrismLauncher/releases/latest)

- Download Modpack Update.zip from [here](https://gitlab.com/salieribastun/easycreate/-/raw/main/Modpack%20Update.zip). This is a minimal installation with Quilt and the latest packwiz-bootstrap, which will auto-update your mod installation on Minecraft 1.19.2

- Install Modpack Update using Prism Launcher.
    - Add Instance
    - Import from ZIP
    - Your Download Directory/Modpack Update.zip
    - OK

## Server Installation

- Download and install Docker following the instructions given [here (look for Server)](https://docs.docker.com/engine/install/)

- Create a new user who can access Docker in some fashion (EG: `sudo` or `docker` group access)

- Make a directory somewhere you can find in the future, and use `chown` to change the ownership to the aforementioned group. Example: `/home/<user>/data`, `/media/data`

- Navigate to the `data` directory using `cd` and use `nano` to create a new file named `docker-compose.yml`, then paste the following:
```docker
version: '3.3'
services:
    minecraft-server:
        ports:
            - '25565:25565'
        volumes:
            - '/home/<user>/data(changethis):/data'
        environment:
            - EULA=TRUE
            - TYPE=QUILT
            - VERSION=1.19.2
            - 'PACKWIZ_URL=https://gitlab.com/salieribastun/easycreate/-/raw/main/pack.toml'
        image: itzg/minecraft-server
```
- Save using `Ctrl+X` then press `Y` to confirm your changes.

- `sudo docker compose up -d` (or `docker compose up -d` if you placed your user in the `docker` group)

- Wait a bit, then try connecting to your server.

## Usage
Open Modpack Update in Prism Launcher and enjoy!

## Contributing
Contributions are welcome, removal/addition of content will be reviewed if the modpack is available on CurseForge or Modrinth.
